//
//  Question.swift
//  QuizApp
//
//  Created by iOS Dev2 on 09/04/20.
//  Copyright © 2020 iOS Dev2. All rights reserved.
//

import Foundation
class Question {
    
    let questionText:String
    let answer:Bool
    
    init(text:String,correstAnswer:Bool) {
        
        questionText = text
        answer = correstAnswer
    }
}
