//
//  QuestionBank.swift
//  QuizApp
//
//  Created by iOS Dev2 on 10/04/20.
//  Copyright © 2020 iOS Dev2. All rights reserved.
//

import Foundation
class QuestionBank {
    var list = [Question]()

init() {
      
let item = Question(text: "1.Electrons are larger than molecules.", correstAnswer: false)
    list.append(item)
    list.append(Question(text: "2.The Atlantic Ocean is the biggest ocean on Earth.", correstAnswer: false))
    list.append(Question(text: "3.The chemical make up food often changes when you cook it.", correstAnswer: true))
    list.append(Question(text: "4.Sharks are mammals.", correstAnswer: false))
    list.append(Question(text: "5.The human body has four lungs.", correstAnswer: false))
    list.append(Question(text: "6.Atoms are most stable when their outer shells are full.", correstAnswer: true))
    list.append(Question(text: "7.Filtration separates mixtures based upon their particle size.", correstAnswer: true))
    list.append(Question(text: "8.Venus is the closest planet to the Sun.", correstAnswer: false))
    list.append(Question(text: "9.Conductors have low resistance.", correstAnswer: true))
    list.append(Question(text: "10.Molecules can have atoms from more than one chemical element.", correstAnswer: true))
    list.append(Question(text: "11.Water is an example of a chemical element.", correstAnswer: false))
    list.append(Question(text: "12.The study of plants is known as botany.", correstAnswer: true))
    list.append(Question(text: "13.Kelvin is a measure of temperature.", correstAnswer: true))

    }
    
}
